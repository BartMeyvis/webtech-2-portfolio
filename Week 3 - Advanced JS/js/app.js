/* ---------------------------------------------------- APP ---------------------------------- */
$("#todo-item1").click(function(){ 								// make sure we can click on an element
    $(this).toggleClass("done"); 						// when we click on any list item, add the class done if it doesn't exist yet on the element or remove it when it exists
});


var priority = 1;

$("#btn-high").click(function(){
	document.getElementById("btn-high").className = "btn btn-high selected";
	document.getElementById("btn-medium").className = "btn btn-medium";
	document.getElementById("btn-low").className = "btn btn-low";
	priority = 1;
});
$("#btn-medium").click(function(){
	document.getElementById("btn-high").className = "btn btn-high";
	document.getElementById("btn-medium").className = "btn btn-medium selected";
	document.getElementById("btn-low").className = "btn btn-low";
	priority = 2;
});
$("#btn-low").click(function(){
	document.getElementById("btn-high").className = "btn btn-high";
	document.getElementById("btn-medium").className = "btn btn-medium";
	document.getElementById("btn-low").className = "btn btn-low selected";
	priority = 3;
});

$("#add-item-text").keyup(function(e){ 					// make sure we can detect a keyup event
	if(e.which === 13)
	{

		// this is the ENTER key with code 13
		var todoText = $(this).val();
		var li = document.createElement("li");			// create a new list item in memory
		li.innerHTML = todoText;

		// put some text inside of the <li> tags
		switch(priority)
		{
			case 1:
				li.className = "prior-high";
				break;
			case 2:
				li.className = "prior-medium";
				break;
			case 3:
				li.className = "prior-low";
				break;
		}
							// give it the class prior-high by default

		$(this).val(""); 								// clear the input field by setting the value to ""

		$("#todo-list").prepend(li); 					// add the newly created list item to the ul#todo-list
		$(li).click(function(){							// make sure that we can click on the newly created list item
			$(this).toggleClass("done"); 				// toggle the class done when clicking on the element
		});
	}
});