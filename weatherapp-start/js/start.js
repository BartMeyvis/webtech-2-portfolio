/*global  $, Skycons*/
(function () {
    'use strict';

    //jSon
    var App = {
        APIKEY: "b368f5ea9064a2755b3212a7dcefe8c0",
        lat: "",
        lng: "",

        init: function () {
            // kickstart the app
            App.getLocation();
        },
        getLocation: function () {
            // get the current user position	App.foundPos maakt nieuwe functie
            navigator.geolocation.getCurrentPosition(App.foundPosition);
        },
        getDay: function (plus){
            var d = new Date();
            var n = d.getDay();

            var day = n + plus;
            if(day > 6){
                day = day - 7;
            }
            switch(day) {
                case 0 :
                    return "Sunday"
                    break;
                case 1 :
                    return "Monday"
                    break;
                case 2 :
                    return "Tuesday"
                    break;
                case 3 :
                    return "Wednesday"
                    break;
                case 4 :
                    return "Thursday"
                    break;
                case 5 :
                    return "Friday"
                    break;
                case 6 :
                    return "Saturday"
                    break;
            }
        },
        foundPosition: function (pos) {
            // found the current user position
            App.lat = pos.coords.latitude;
            App.lng = pos.coords.longitude;
            App.getWeather();

        },
        getWeather: function () {
            // get current weather for my position.
            var url = "https://api.forecast.io/forecast/" + App.APIKEY + "/" + App.lat + "," + App.lng;
            var skycons = new Skycons({"color": "black"});
            //JSONP
            window.jQuery.ajax({
                url: url,
                dataType: "jsonp",
                success: function (data) {

                    skycons.add("weather-icon",data.currently.icon );


                    var Ftemp = data.currently.temperature;
                    var Ctemp = parseInt((Ftemp-32)/1.8);
                    $("#today-temp").text(Ctemp+"°");


                    skycons.add("weatherDay1", data.daily.data[1].icon);


                    $("#nameDay1").text(App.getDay(1));


                    skycons.add("weatherDay2", data.daily.data[2].icon);


                    $("#nameDay2").text(App.getDay(2));


                    skycons.add("weatherDay3", data.daily.data[3].icon);


                    $("#nameDay3").text(App.getDay(3));


                    skycons.add("weatherDay4", data.daily.data[4].icon);


                    $("#nameDay4").text(App.getDay(4));


                    skycons.add("weatherDay5", data.daily.data[5].icon);


                    $("#nameDay5").text(App.getDay(5));

                    switch(data.currently.icon) {

                        case "clear-day" :
                            $(".weather-summary-part1").text("I fucking");
                            localStorage.setItem("today-summary-1","I fucking");
                            $(".weather-summary-part2").text("LOVE");
                            localStorage.setItem("today-summary-2","LOVE");
                            $(".weather-summary-part3").text("the sun.");
                            localStorage.setItem("today-summary-3","the sun.");



                            break;
                        case "rain":

                            $(".weather-summary-part1").text("It's fucking");
                            localStorage.setItem("today-summary-1","It's fucking");
                            $(".weather-summary-part2").text("RAINING");
                            localStorage.setItem("today-summary-2","RAINING");
                            $(".weather-summary-part3").text("now.");
                            localStorage.setItem("today-summary-3","now.");
                            break;
                    }
                    $(".foo").text(data.currently.summary);

                    skycons.play();
                    var timestamp = new Date().getTime();
                    localStorage.setItem("setTimeStamp", timestamp);
                    localStorage.setItem("data", data);
                    localStorage.setItem("today-icon",data.currently.icon);
                    localStorage.setItem("today-temp",Ctemp+"°");
                    localStorage.setItem("day1-icon",data.daily.data[1].icon);
                    localStorage.setItem("day1-name",App.getDay(1));
                    localStorage.setItem("day2-icon",data.daily.data[2].icon);
                    localStorage.setItem("day2-name",App.getDay(2));
                    localStorage.setItem("day3-icon",data.daily.data[3].icon);
                    localStorage.setItem("day3-name",App.getDay(3));
                    localStorage.setItem("day4-icon",data.daily.data[4].icon);
                    localStorage.setItem("day4-name",App.getDay(4));
                    localStorage.setItem("day5-icon",data.daily.data[5].icon);
                    localStorage.setItem("day5-name",App.getDay(5));



                }
            });
        }
    };

    var timestampNow = new Date().getTime();

    if(localStorage.getItem("setTimeStamp") === null){
        App.init();
    }else if(((localStorage.getItem("setTimeStamp"))+3600000)>timestampNow){
        //user local
        var skycons = new Skycons({"color": "red"});
        var test = localStorage.getItem("data");
        skycons.add("weather-icon", test.currently.icon);
        $("#today-temp").text(localStorage.getItem("today-temp"));
        skycons.add("weatherDay1", localStorage.getItem("day1-icon"));
        $("#nameDay1").text(localStorage.getItem("day1-name"));
        skycons.add("weatherDay2", localStorage.getItem("day2-icon"));
        $("#nameDay2").text(localStorage.getItem("day2-name"));
        skycons.add("weatherDay3", localStorage.getItem("day3-icon"));
        $("#nameDay3").text(localStorage.getItem("day3-name"));
        skycons.add("weatherDay4", localStorage.getItem("day4-icon"));
        $("#nameDay4").text(localStorage.getItem("day4-name"));
        skycons.add("weatherDay5", localStorage.getItem("day5-icon"));
        $("#nameDay5").text(localStorage.getItem("day5-name"));
        skycons.play();
        $(".weather-summary-part1").text(localStorage.getItem("today-summary-1"));
        $(".weather-summary-part2").text(localStorage.getItem("today-summary-2"));
        $(".weather-summary-part3").text(localStorage.getItem("today-summary-3"));

    }else{
        App.init();
    }


}());